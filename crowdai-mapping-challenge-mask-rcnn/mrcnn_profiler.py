import os
import sys
import time
import numpy as np
import skimage.io

# Download and install the Python COCO tools from https://github.com/waleedka/coco
# That's a fork from the original https://github.com/pdollar/coco with a bug
# fix for Python 3.
# I submitted a pull request https://github.com/cocodataset/cocoapi/pull/50
# If the PR is merged then use the original repo.
# Note: Edit PythonAPI/Makefile and replace "python" with "python3".
#  
# A quick one liner to install the library 
# !pip install git+https://github.com/waleedka/coco.git#subdirectory=PythonAPI

from pycocotools.coco import COCO
from cocoeval import COCOeval
# from pycocotools.cocoeval import COCOeval
from pycocotools import mask as maskUtils

import coco #a slightly modified version

from mrcnn.evaluate import build_coco_results, evaluate_coco
from mrcnn.dataset import MappingChallengeDataset
from mrcnn import visualize


import zipfile
import urllib.request
import shutil
import glob
import tqdm
import random
import time

from tensorflow.python.client import timeline
import tensorflow as tf

ROOT_DIR = os.getcwd()

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import model as modellib, utils

############ PARAMETERS ################
DATASET_NAME = "data"
# DATASET_NAME = "data_small"
########################################

### baseline mask r-cnn model with resnet backbone (pretrained by crowdai)
training_stamp = "original-from-crowdai"
epoch_num = 0
PRETRAINED_MODEL_PATH = os.path.join(ROOT_DIR, "pretrained_models", "pretrained_weights.h5")
model_type = "resnet50"

### mask r-cnn model with mobilenet-v1 backbone, pretrained only on imagenet
# training_stamp = "original-from-imagenet"
# epoch_num = 0
# alpha = "1_0"
# PRETRAINED_MODEL_PATH = os.path.join(ROOT_DIR, "pretrained_models", "mobilenet_1_0_224_tf_no_top.h5")

### mask r-cnn model with mobilenet-v1 backbone
# training_stamp = "20181204T2314"
# epoch_num = 40
# alpha = "1_0"

# training_stamp = "20181207T2229"
# epoch_num = 100
# alpha = "1_0"

# training_stamp = "20181208T2240"
# epoch_num = 100
# alpha = "2_5"

# PRETRAINED_MODEL_PATH = os.path.join(ROOT_DIR, "logs", "crowdai-mapping-challenge{}".format(training_stamp), "mask_rcnn_crowdai-mapping-challenge_{}.h5".format(str(epoch_num).zfill(4)))
# model_type = "mobilenet224v1_{}".format(alpha)

model_name = "{model_type}_{epoch_num}".format(model_type=model_type, epoch_num=epoch_num)

LOGS_DIRECTORY = os.path.join(ROOT_DIR, "logs")
MODEL_DIR = os.path.join(ROOT_DIR, "logs")
IMAGE_DIR = os.path.join(ROOT_DIR, "data", "val", "images")
ANNOTATION_PATH = os.path.join(ROOT_DIR, "data", "val", "annotation.json")

class InferenceConfig(coco.CocoConfig):
    # Set batch size to 1 since we'll be running inference on
    # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
    NUM_CLASSES = 1 + 1  # 1 Background + 1 Building
    IMAGE_MAX_DIM=320
    IMAGE_MIN_DIM=320
    NAME = "crowdai-mapping-challenge"
    BACKBONE = model_type # [resnet50, mobilenet224v1]
config = InferenceConfig()
config.display()

run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
run_metadata = tf.RunMetadata()
model = modellib.MaskRCNN(mode="inference", model_dir=MODEL_DIR, config=config,
                            options=run_options, run_metadata=run_metadata)

model_path = PRETRAINED_MODEL_PATH
model.load_weights(model_path, by_name=True)

class_names = ['BG', 'building'] # In our case, we have 1 class for the background, and 1 class for building

# Show mask on some default validation image
file_names = next(os.walk(IMAGE_DIR))[2]
image = skimage.io.imread(os.path.join(IMAGE_DIR, "000000000000.jpg"))

# Gather all JPG files in the test set as small batches
files = glob.glob(os.path.join(IMAGE_DIR, "*.jpg"))

##########################################################################################################
##########################################################################################################
# Inference Test
#
filename = files[0]
# filename = tqdm.tqdm(ALL_FILES)[0]
img = [skimage.io.imread(filename)]

# Run model in your usual way
# predictions, times = model.compile(learning_rate=0.1, momentum=0.1)
model.compile_inference()
num_iterations = 10
for i in range(num_iterations):
    predictions, times = model.detect(img, verbose=0)
# predictions, times = model.detect(img, verbose=0)
trace = timeline.Timeline(step_stats=run_metadata.step_stats)
with open('timeline.ctf.json', 'w') as f:
    f.write(trace.generate_chrome_trace_format())
