import os
import sys
import time
import numpy as np
import skimage.io

# Download and install the Python COCO tools from https://github.com/waleedka/coco
# That's a fork from the original https://github.com/pdollar/coco with a bug
# fix for Python 3.
# I submitted a pull request https://github.com/cocodataset/cocoapi/pull/50
# If the PR is merged then use the original repo.
# Note: Edit PythonAPI/Makefile and replace "python" with "python3".
#  
# A quick one liner to install the library 
# !pip install git+https://github.com/waleedka/coco.git#subdirectory=PythonAPI

from pycocotools.coco import COCO
from cocoeval import COCOeval
# from pycocotools.cocoeval import COCOeval
from pycocotools import mask as maskUtils


import coco #a slightly modified version

from mrcnn.evaluate import build_coco_results, evaluate_coco
from mrcnn.dataset import MappingChallengeDataset
from mrcnn import visualize


import zipfile
import urllib.request
import shutil
import glob
import tqdm
import random
import time

import matplotlib.pyplot as plt

ROOT_DIR = os.getcwd()

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import model as modellib, utils

############ PARAMETERS ################
DATASET_NAME = "data"
# DATASET_NAME = "data_small"
########################################

### baseline mask r-cnn model with resnet backbone (pretrained by crowdai)
# training_stamp = "original-from-crowdai"
# epoch_num = 0
# PRETRAINED_MODEL_PATH = os.path.join(ROOT_DIR, "pretrained_models", "pretrained_weights.h5")
# model_type = "resnet50"

### mask r-cnn model with mobilenet-v1 backbone, pretrained only on imagenet
# training_stamp = "original-from-imagenet"
# epoch_num = 0
# alpha = "1_0"
# PRETRAINED_MODEL_PATH = os.path.join(ROOT_DIR, "pretrained_models", "mobilenet_1_0_224_tf_no_top.h5")

### mask r-cnn model with mobilenet-v1 backbone
# training_stamp = "20181204T2314"
# epoch_num = 40
# alpha = "1_0"

# training_stamp = "20181207T2229"
# epoch_num = 100
# alpha = "1_0"

training_stamp = "20181208T2240"
epoch_num = 100
alpha = "2_5"

PRETRAINED_MODEL_PATH = os.path.join(ROOT_DIR, "logs", "crowdai-mapping-challenge{}".format(training_stamp), "mask_rcnn_crowdai-mapping-challenge_{}.h5".format(str(epoch_num).zfill(4)))
model_type = "mobilenet224v1_{}".format(alpha)

model_name = "{model_type}_{epoch_num}".format(model_type=model_type, epoch_num=epoch_num)

LOGS_DIRECTORY = os.path.join(ROOT_DIR, "logs")
MODEL_DIR = os.path.join(ROOT_DIR, "logs")
IMAGE_DIR = os.path.join(ROOT_DIR, "data", "val", "images")
ANNOTATION_PATH = os.path.join(ROOT_DIR, "data", "val", "annotation.json")

class InferenceConfig(coco.CocoConfig):
    # Set batch size to 1 since we'll be running inference on
    # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
    NUM_CLASSES = 1 + 1  # 1 Background + 1 Building
    IMAGE_MAX_DIM=320
    IMAGE_MIN_DIM=320
    NAME = "crowdai-mapping-challenge"
    BACKBONE = model_type # [resnet50, mobilenet224v1]
config = InferenceConfig()
config.display()

model = modellib.MaskRCNN(mode="inference", model_dir=MODEL_DIR, config=config)

model_path = PRETRAINED_MODEL_PATH
model.load_weights(model_path, by_name=True)

class_names = ['BG', 'building'] # In our case, we have 1 class for the background, and 1 class for building

# Gather all JPG files in the test set as small batches
files = glob.glob(os.path.join(IMAGE_DIR, "*.jpg"))
ALL_FILES=[]
_buffer = []
for _idx, _file in enumerate(files):
    if len(_buffer) == config.IMAGES_PER_GPU * config.GPU_COUNT:
        ALL_FILES.append(_buffer)
        _buffer = []
    else:
        _buffer.append(_file)

if len(_buffer) > 0:
    ALL_FILES.append(_buffer)

######################################################################
SMOOTH = 1e-6
def iou_numpy(outputs, labels):
    intersection = np.sum(outputs & labels)
    union = np.sum(outputs | labels)
    
    iou = (intersection + SMOOTH) / (union + SMOOTH)
    
    return iou

    thresholded = np.ceil(np.clip(20 * (iou - 0.5), 0, 10)) / 10
    
    return thresholded  # Or thresholded.mean()


VAL_ANNOTATIONS_PATH = "data/val/annotation.json"
coco = COCO(VAL_ANNOTATIONS_PATH)

# Iterate over all the batches and predict
plt.figure()
_final_object = []
img_count = 0
iou_sum = 0.0
for files in tqdm.tqdm(ALL_FILES):
    img_count += 1
    images = [skimage.io.imread(x) for x in files]
    predictions, _ = model.detect(images, verbose=0)
    true_mask = np.zeros((300,300), dtype=np.uint8)
    mask = np.zeros((300,300), dtype=np.uint8)
    image_id = int(files[0].split('/')[-1].split('.')[0])
    annotation_ids = coco.getAnnIds(imgIds=image_id)
    annotations = coco.loadAnns(annotation_ids)
    for _idx, annotation in enumerate(annotations):
        rle = maskUtils.frPyObjects(annotation['segmentation'], 300, 300)
        m = maskUtils.decode(rle)
        true_mask = np.logical_or(true_mask, m.reshape((300, 300)))
    for _idx, r in enumerate(predictions):
        _file = files[_idx]
        image_id = int(_file.split("/")[-1].replace(".jpg",""))
        for _idx, class_id in enumerate(r["class_ids"]):
            if class_id == 1:
                mask = np.logical_or(mask, r["masks"].astype(np.uint8)[:, :, _idx])
    
    iou = iou_numpy(mask, true_mask)
    iou_sum += iou

    print(iou)
    masks = np.hstack([mask, true_mask])
    plt.imshow(masks)
    plt.pause(1)
    # if iou < 0.1:
        # plt.pause(2)

    if img_count > 1000:
        break

print(model_name)
print("Ave IoU: {}".format(iou_sum / img_count))

# resnet50_0
# Ave IoU: 0.48620507202053104

# mobilenet224v1_1_0_0
# Ave IoU: 0.1519179783566399

# mobilenet224v1_1_0_40
# Ave IoU: 0.31468044590661054

# mobilenet224v1_1_0_100
# Ave IoU: 0.3558135482152028

# mobilenet224v1_2_5_100
# Ave IoU: 0.5548640956625044
###################################################
