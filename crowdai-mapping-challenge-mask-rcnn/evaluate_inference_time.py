import os
import sys
import time
import numpy as np
import skimage.io

# Download and install the Python COCO tools from https://github.com/waleedka/coco
# That's a fork from the original https://github.com/pdollar/coco with a bug
# fix for Python 3.
# I submitted a pull request https://github.com/cocodataset/cocoapi/pull/50
# If the PR is merged then use the original repo.
# Note: Edit PythonAPI/Makefile and replace "python" with "python3".
#  
# A quick one liner to install the library 
# !pip install git+https://github.com/waleedka/coco.git#subdirectory=PythonAPI

from pycocotools.coco import COCO
from cocoeval import COCOeval
# from pycocotools.cocoeval import COCOeval
from pycocotools import mask as maskUtils

import coco #a slightly modified version

from mrcnn.evaluate import build_coco_results, evaluate_coco
from mrcnn.dataset import MappingChallengeDataset
from mrcnn import visualize


import zipfile
import urllib.request
import shutil
import glob
import tqdm
import random
import time

ROOT_DIR = os.getcwd()

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import model as modellib, utils

############ PARAMETERS ################
DATASET_NAME = "data"
# DATASET_NAME = "data_small"
########################################

### baseline mask r-cnn model with resnet backbone (pretrained by crowdai)
# training_stamp = "original-from-crowdai"
# epoch_num = 0
# PRETRAINED_MODEL_PATH = os.path.join(ROOT_DIR, "pretrained_models", "pretrained_weights.h5")
# model_type = "resnet50"

### mask r-cnn model with mobilenet-v1 backbone, pretrained only on imagenet
# training_stamp = "original-from-imagenet"
# epoch_num = 0
# alpha = "1_0"
# PRETRAINED_MODEL_PATH = os.path.join(ROOT_DIR, "pretrained_models", "mobilenet_1_0_224_tf_no_top.h5")

### mask r-cnn model with mobilenet-v1 backbone
# training_stamp = "20181204T2314"
# epoch_num = 40
# alpha = "1_0"

training_stamp = "20181207T2229"
epoch_num = 100
alpha = "1_0"

# training_stamp = "20181208T2240"
# epoch_num = 100
# alpha = "2_5"

PRETRAINED_MODEL_PATH = os.path.join(ROOT_DIR, "logs", "crowdai-mapping-challenge{}".format(training_stamp), "mask_rcnn_crowdai-mapping-challenge_{}.h5".format(str(epoch_num).zfill(4)))
model_type = "mobilenet224v1_{}".format(alpha)

model_name = "{model_type}_{epoch_num}".format(model_type=model_type, epoch_num=epoch_num)

LOGS_DIRECTORY = os.path.join(ROOT_DIR, "logs")
MODEL_DIR = os.path.join(ROOT_DIR, "logs")
IMAGE_DIR = os.path.join(ROOT_DIR, "data", "val", "images")
ANNOTATION_PATH = os.path.join(ROOT_DIR, "data", "val", "annotation.json")

class InferenceConfig(coco.CocoConfig):
    # Set batch size to 1 since we'll be running inference on
    # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
    NUM_CLASSES = 1 + 1  # 1 Background + 1 Building
    IMAGE_MAX_DIM=320
    IMAGE_MIN_DIM=320
    NAME = "crowdai-mapping-challenge"
    BACKBONE = model_type # [resnet50, mobilenet224v1]
config = InferenceConfig()
config.display()

model = modellib.MaskRCNN(mode="inference", model_dir=MODEL_DIR, config=config)

model_path = PRETRAINED_MODEL_PATH
model.load_weights(model_path, by_name=True)

class_names = ['BG', 'building'] # In our case, we have 1 class for the background, and 1 class for building

# Show mask on some default validation image
file_names = next(os.walk(IMAGE_DIR))[2]
image = skimage.io.imread(os.path.join(IMAGE_DIR, "000000000000.jpg"))

# Gather all JPG files in the test set as small batches
files = glob.glob(os.path.join(IMAGE_DIR, "*.jpg"))

##########################################################################################################
##########################################################################################################
# Inference Test
#
filename = files[0]
img = [skimage.io.imread(filename)]

num_iterations = 50
times_arr = np.empty((num_iterations, 5))
for i in range(num_iterations):
    predictions, times = model.detect(img, verbose=0)
    times_arr[i,:] = times

total_times = times_arr[1:,-1]
total_time_mean = np.mean(total_times)
total_time_std = np.std(total_times)
fwd_pass_times = times_arr[1:,1]
fwd_pass_time_mean = np.mean(fwd_pass_times)
fwd_pass_time_std = np.std(fwd_pass_times)
print(model_name)
print("Whole inference took {total_time_mean:.3f} +- {total_time_std:.5f} sec.".format(total_time_mean=total_time_mean, total_time_std=total_time_std))
print("Fwd Pass took {fwd_pass_time_mean:.3f} +- {fwd_pass_time_std:.5f} sec.".format(fwd_pass_time_mean=fwd_pass_time_mean, fwd_pass_time_std=fwd_pass_time_std))
# print(np.mean(np.mean(times_arr[1:,-1], axis=0)))

# [7.87704818e-03 8.84976679e-02 3.84632422e-05 9.37183049e-03
#  1.05785010e-01]
# [8.11644445e-04 4.11092350e-03 4.68056164e-06 4.21791307e-04
#  3.92946582e-03]

# resnet50_0
# Whole inference took 0.131 +- 0.00551 sec.

# # imagenet pretrained
# mobilenet224v1_1_0_0
# Whole inference took 0.127 +- 0.00320 sec.

# # 40 epochs (head) alpha 1.0
# mobilenet224v1_1_0_40
# Whole inference took 0.106 +- 0.00441 sec.

# # 100 epochs alpha 1.0
# mobilenet224v1_1_0_100
# Whole inference took 0.100 +- 0.00344 sec.

# # 100 epochs alpha 0.25
# mobilenet224v1_2_5_100
# Whole inference took 0.102 +- 0.00310 sec.

# >>> rate_from_ms(0.131, 0.00551)
# (7.633587786259541, 0.33517466493178727)
# >>> rate_from_ms(0.127, 0.00320)
# (7.874015748031496, 0.20352867846284894)
# >>> rate_from_ms(0.106, 0.00441)
# (9.433962264150944, 0.40952626818491566)
# >>> rate_from_ms(0.100, 0.00344)
# (10.0, 0.3562551781275882)
# >>> rate_from_ms(0.102, 0.00310)
# (9.803921568627452, 0.30730188941097225)


##########################################################################################################
##########################################################################################################

def rate_from_ms(mean, std):
    return 1/mean, max(abs(1/(mean+std) - 1/mean), abs(1/(mean-std) - 1/mean))