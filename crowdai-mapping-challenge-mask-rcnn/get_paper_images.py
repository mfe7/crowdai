import os
import sys
import time
import numpy as np
import skimage.io

# Download and install the Python COCO tools from https://github.com/waleedka/coco
# That's a fork from the original https://github.com/pdollar/coco with a bug
# fix for Python 3.
# I submitted a pull request https://github.com/cocodataset/cocoapi/pull/50
# If the PR is merged then use the original repo.
# Note: Edit PythonAPI/Makefile and replace "python" with "python3".
#  
# A quick one liner to install the library 
# !pip install git+https://github.com/waleedka/coco.git#subdirectory=PythonAPI

from pycocotools.coco import COCO
from cocoeval import COCOeval
# from pycocotools.cocoeval import COCOeval
from pycocotools import mask as maskUtils

import coco #a slightly modified version

from mrcnn.evaluate import build_coco_results, evaluate_coco
from mrcnn.dataset import MappingChallengeDataset
from mrcnn import visualize

import zipfile
import urllib.request
import shutil
import glob
import tqdm
import random
import time

import skimage.transform

import matplotlib.pyplot as plt

ROOT_DIR = os.getcwd()

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import model as modellib, utils

############ PARAMETERS ################
DATASET_NAME = "data"
########################################

models = []

### baseline mask r-cnn model with resnet backbone (pretrained by crowdai)
training_stamp = "original-from-crowdai"
epoch_num = 0
PRETRAINED_MODEL_PATH = os.path.join(ROOT_DIR, "pretrained_models", "pretrained_weights.h5")
model_type = "resnet50"
model_name = "{model_type}_{epoch_num}".format(model_type=model_type, epoch_num=epoch_num)
info = [training_stamp, epoch_num, PRETRAINED_MODEL_PATH, model_type, model_name]
models.append(info)

### mask r-cnn model with mobilenet-v1 backbone, pretrained only on imagenet
training_stamp = "original-from-imagenet"
epoch_num = 0
alpha = "1_0"
PRETRAINED_MODEL_PATH = os.path.join(ROOT_DIR, "pretrained_models", "mobilenet_1_0_224_tf_no_top.h5")
model_type = "mobilenet224v1_{}".format(alpha)
model_name = "{model_type}_{epoch_num}".format(model_type=model_type, epoch_num=epoch_num)
info = [training_stamp, epoch_num, PRETRAINED_MODEL_PATH, model_type, model_name]
models.append(info)

### mask r-cnn model with mobilenet-v1 backbone
training_stamp = "20181204T2314"
epoch_num = 40
alpha = "1_0"
model_type = "mobilenet224v1_{}".format(alpha)
PRETRAINED_MODEL_PATH = os.path.join(ROOT_DIR, "logs", "crowdai-mapping-challenge{}".format(training_stamp), "mask_rcnn_crowdai-mapping-challenge_{}.h5".format(str(epoch_num).zfill(4)))
model_name = "{model_type}_{epoch_num}".format(model_type=model_type, epoch_num=epoch_num)
info = [training_stamp, epoch_num, PRETRAINED_MODEL_PATH, model_type, model_name]
models.append(info)

training_stamp = "20181207T2229"
epoch_num = 100
alpha = "1_0"
model_type = "mobilenet224v1_{}".format(alpha)
PRETRAINED_MODEL_PATH = os.path.join(ROOT_DIR, "logs", "crowdai-mapping-challenge{}".format(training_stamp), "mask_rcnn_crowdai-mapping-challenge_{}.h5".format(str(epoch_num).zfill(4)))
model_name = "{model_type}_{epoch_num}".format(model_type=model_type, epoch_num=epoch_num)
info = [training_stamp, epoch_num, PRETRAINED_MODEL_PATH, model_type, model_name]
models.append(info)

training_stamp = "20181208T2240"
epoch_num = 100
alpha = "2_5"
model_type = "mobilenet224v1_{}".format(alpha)
PRETRAINED_MODEL_PATH = os.path.join(ROOT_DIR, "logs", "crowdai-mapping-challenge{}".format(training_stamp), "mask_rcnn_crowdai-mapping-challenge_{}.h5".format(str(epoch_num).zfill(4)))
model_name = "{model_type}_{epoch_num}".format(model_type=model_type, epoch_num=epoch_num)
info = [training_stamp, epoch_num, PRETRAINED_MODEL_PATH, model_type, model_name]
models.append(info)


LOGS_DIRECTORY = os.path.join(ROOT_DIR, "logs")
MODEL_DIR = os.path.join(ROOT_DIR, "logs")
IMAGE_DIR = os.path.join(ROOT_DIR, "data", "val", "images")
ANNOTATION_PATH = os.path.join(ROOT_DIR, "data", "val", "annotation.json")
class_names = ['BG', 'building'] # In our case, we have 1 class for the background, and 1 class for building

# if you wanna use validation set.....
# img_paths = [os.path.join(IMAGE_DIR, str(i).zfill(12)+".jpg") for i in range(5)]

# if you wanna use mars set.....
IMAGE_DIR = os.path.join(ROOT_DIR, "../mapping-challenge-starter-kit/crowdai/crowdai_data/crowdai_data_voc/JPEGImages/from_google_maps")
img_paths = [os.path.join(IMAGE_DIR, i+".png") for i in ["mars", "maps_0000", "maps_0001"]]

for model in models:
    training_stamp, epoch_num, PRETRAINED_MODEL_PATH, model_type, model_name = model

    class InferenceConfig(coco.CocoConfig):
        # Set batch size to 1 since we'll be running inference on
        # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
        GPU_COUNT = 1
        IMAGES_PER_GPU = 1
        NUM_CLASSES = 1 + 1  # 1 Background + 1 Building
        IMAGE_MAX_DIM=320
        IMAGE_MIN_DIM=320
        NAME = "crowdai-mapping-challenge"
        BACKBONE = model_type # [resnet50, mobilenet224v1]
    config = InferenceConfig()
    config.display()

    model = modellib.MaskRCNN(mode="inference", model_dir=MODEL_DIR, config=config)
    model_path = PRETRAINED_MODEL_PATH
    model.load_weights(model_path, by_name=True)

    for img_path in img_paths:
        img = skimage.io.imread(img_path)
        # img = skimage.transform.resize(img, (320,320))
        img = [img]
        predictions, times = model.detect(img, verbose=0)
        p = predictions[0]
        mask = visualize.display_instances(img[0], p['rois'], p['masks'], p['class_ids'], 
                                    class_names, p['scores'])
        mask_filename = "{img_path}_{model_name}.png".format(img_path=img_path.split('/')[-1].split('.')[0], model_name=model_name)
        plt.imsave(mask_filename, mask)

