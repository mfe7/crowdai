import os
import sys
import time
import numpy as np

# Download and install the Python COCO tools from https://github.com/waleedka/coco
# That's a fork from the original https://github.com/pdollar/coco with a bug
# fix for Python 3.
# I submitted a pull request https://github.com/cocodataset/cocoapi/pull/50
# If the PR is merged then use the original repo.
# Note: Edit PythonAPI/Makefile and replace "python" with "python3".
#  
# A quick one liner to install the library 
# !pip install git+https://github.com/waleedka/coco.git#subdirectory=PythonAPI

from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
from pycocotools import mask as maskUtils

from mrcnn.evaluate import build_coco_results, evaluate_coco
from mrcnn.dataset import MappingChallengeDataset

import zipfile
import urllib.request
import shutil

import importlib

ROOT_DIR = os.getcwd()

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import model as modellib, utils

############ PARAMETERS ################
alpha = "2_5"
model_type = "mobilenet224v1_{alpha}".format(alpha=alpha)

DATASET_NAME = "data"
# DATASET_NAME = "data_small"
########################################

if "mobilenet224v1" in model_type:
    # https://github.com/fchollet/deep-learning-models/releases/download/v0.6/mobilenet_1_0_224_tf_no_top.h5
    pretrained_weights_filename = "mobilenet_{alpha}_224_tf_no_top.h5".format(alpha=alpha)
elif model_type == "resnet50":
    pretrained_weights_filename = "pretrained_weights.h5"

TRAINING_SET_PATH = os.path.join(DATASET_NAME, "train")
VAL_SET_PATH = os.path.join(DATASET_NAME, "val")
PRETRAINED_MODEL_PATH = os.path.join(ROOT_DIR, "pretrained_models/", pretrained_weights_filename)
LOGS_DIRECTORY = os.path.join(ROOT_DIR, "logs")

class MappingChallengeConfig(Config):
    """Configuration for training on data in MS COCO format.
    Derives from the base Config class and overrides values specific
    to the COCO dataset.
    """
    # Give the configuration a recognizable name
    NAME = "crowdai-mapping-challenge"

    # Original Mask-RCNN authors used a GPU with 12GB memory, which can fit two images.
    # Adjust down if you use a smaller GPU.
    # MFE used 1 to fit on a GPU with 6GB memory... 2 didn't seem to work.
    IMAGES_PER_GPU = 1

    # Uncomment to train on 8 GPUs (default is 1)
    GPU_COUNT = 1

    # Number of classes (including background)
    NUM_CLASSES = 1 + 1  # 1 Backgroun + 1 Building

    STEPS_PER_EPOCH=1000
    VALIDATION_STEPS=50

    IMAGE_MAX_DIM=320
    IMAGE_MIN_DIM=320
    
    BACKBONE = model_type # [resnet50, mobilenet224v1]
    
config = MappingChallengeConfig()
config.display()

model = modellib.MaskRCNN(mode="training", config=config, model_dir=LOGS_DIRECTORY)

# Load pretrained weights
model_path = PRETRAINED_MODEL_PATH
model.load_weights(model_path, by_name=True)

# Load training dataset
dataset_train = MappingChallengeDataset()
dataset_train.load_dataset(dataset_dir=TRAINING_SET_PATH, load_small=True)
dataset_train.prepare()

# Load validation dataset
dataset_val = MappingChallengeDataset()
val_coco = dataset_val.load_dataset(dataset_dir=VAL_SET_PATH, load_small=True, return_coco=True)
dataset_val.prepare()

# *** This training schedule is an example. Update to your needs ***

# Training - Stage 1
# print("Training network heads")
# model.train(dataset_train, dataset_val,
#             learning_rate=config.LEARNING_RATE,
#             epochs=40,
#             layers='heads')

# # Training - Stage 2
# # Finetune layers from ResNet stage 4 and up
# if config.BACKBONE == "mobilenet224v1":
#     stage_2_layers = '11M+'
# else:
#     stage_2_layers = '4+'

# print("Fine tune Resnet stage 4 and up")
# model.train(dataset_train, dataset_val,
#             learning_rate=config.LEARNING_RATE,
#             epochs=120,
#             layers=stage_2_layers)

# Training - Stage 3
# Fine tune all layers
print("Fine tune all layers")
model.train(dataset_train, dataset_val,
            learning_rate=config.LEARNING_RATE,
            epochs=100,
            layers='all')