{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# mapping-challenge-mask_rcnn-training\n",
    "![CrowdAI-Logo](https://github.com/crowdAI/crowdai/raw/master/app/assets/images/misc/crowdai-logo-smile.svg?sanitize=true)\n",
    "\n",
    "This notebook contains the baseline code for the training a vanilla [Mask RCNN](https://arxiv.org/abs/1703.06870) model for the [crowdAI Mapping Challenge](https://www.crowdai.org/challenges/mapping-challenge).\n",
    "\n",
    "This code is adapted from the [Mask RCNN]() tensorflow implementation available here : [https://github.com/matterport/Mask_RCNN](https://github.com/matterport/Mask_RCNN).\n",
    "\n",
    "First we begin by importing all the necessary dependencies : "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import sys\n",
    "import time\n",
    "import numpy as np\n",
    "\n",
    "# Download and install the Python COCO tools from https://github.com/waleedka/coco\n",
    "# That's a fork from the original https://github.com/pdollar/coco with a bug\n",
    "# fix for Python 3.\n",
    "# I submitted a pull request https://github.com/cocodataset/cocoapi/pull/50\n",
    "# If the PR is merged then use the original repo.\n",
    "# Note: Edit PythonAPI/Makefile and replace \"python\" with \"python3\".\n",
    "#  \n",
    "# A quick one liner to install the library \n",
    "# !pip install git+https://github.com/waleedka/coco.git#subdirectory=PythonAPI\n",
    "\n",
    "from pycocotools.coco import COCO\n",
    "from pycocotools.cocoeval import COCOeval\n",
    "from pycocotools import mask as maskUtils\n",
    "\n",
    "from mrcnn.evaluate import build_coco_results, evaluate_coco\n",
    "from mrcnn.dataset import MappingChallengeDataset\n",
    "\n",
    "import zipfile\n",
    "import urllib.request\n",
    "import shutil\n",
    "\n",
    "import importlib\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dataset location \n",
    "Now we have to download all the files in the datasets section and untar them to have the following structure :\n",
    "```\n",
    "├── data\n",
    "│   ├── test\n",
    "│   │   └── images/\n",
    "│   │   └── annotation.json\n",
    "│   ├── train\n",
    "│   │   └── images/\n",
    "│   │   └── annotation.json\n",
    "│   └── val\n",
    "│       └── images/\n",
    "│       └── annotation.json\n",
    "├── data_small\n",
    "│   ├── train\n",
    "│   │   └── images/    # this only has 1000 images\n",
    "│   │   └── annotation.json\n",
    "│   └── val\n",
    "│       └── images/    # this only has 1000 images\n",
    "│       └── annotation.json\n",
    "├── pretrained_models\n",
    "|   ├── pretrained_weights.h5\n",
    "|   ├── mobilenet_1_0_224_tf_no_top.h5\n",
    "```\n",
    "Note that the `pretrained_weights.h5` (available at [https://www.crowdai.org/challenges/mapping-challenge/dataset_files](https://www.crowdai.org/challenges/mapping-challenge/dataset_files)) are the weights used for the baseline submission, and are obtained by running the learning schedule mentioned later in the experiment. In the said experiment, the initial weights used can be found [here](https://github.com/matterport/Mask_RCNN/releases/download/v2.1/mask_rcnn_balloon.h5). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ROOT_DIR = os.getcwd()\n",
    "\n",
    "# Import Mask RCNN\n",
    "sys.path.append(ROOT_DIR)  # To find local version of the library\n",
    "from mrcnn.config import Config\n",
    "from mrcnn import model as modellib, utils\n",
    "\n",
    "############ PARAMETERS ################\n",
    "model_type = \"mobilenet224v1\"\n",
    "\n",
    "DATASET_NAME = \"data\"\n",
    "# DATASET_NAME = \"data_small\"\n",
    "########################################\n",
    "\n",
    "if model_type == \"mobilenet224v1\":\n",
    "    # https://github.com/fchollet/deep-learning-models/releases/download/v0.6/mobilenet_1_0_224_tf_no_top.h5\n",
    "    pretrained_weights_filename = \"mobilenet_1_0_224_tf_no_top.h5\" \n",
    "elif model_type == \"resnet50\":\n",
    "    pretrained_weights_filename = \"pretrained_weights.h5\"\n",
    "\n",
    "TRAINING_SET_PATH = os.path.join(DATASET_NAME, \"train\")\n",
    "VAL_SET_PATH = os.path.join(DATASET_NAME, \"val\")\n",
    "PRETRAINED_MODEL_PATH = os.path.join(ROOT_DIR, \"pretrained_models/\", pretrained_weights_filename)\n",
    "LOGS_DIRECTORY = os.path.join(ROOT_DIR, \"logs\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Experiment Configuration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class MappingChallengeConfig(Config):\n",
    "    \"\"\"Configuration for training on data in MS COCO format.\n",
    "    Derives from the base Config class and overrides values specific\n",
    "    to the COCO dataset.\n",
    "    \"\"\"\n",
    "    # Give the configuration a recognizable name\n",
    "    NAME = \"crowdai-mapping-challenge\"\n",
    "\n",
    "    # Original Mask-RCNN authors used a GPU with 12GB memory, which can fit two images.\n",
    "    # Adjust down if you use a smaller GPU.\n",
    "    # MFE used 1 to fit on a GPU with 6GB memory... 2 didn't seem to work.\n",
    "    IMAGES_PER_GPU = 1\n",
    "\n",
    "    # Uncomment to train on 8 GPUs (default is 1)\n",
    "    GPU_COUNT = 1\n",
    "\n",
    "    # Number of classes (including background)\n",
    "    NUM_CLASSES = 1 + 1  # 1 Backgroun + 1 Building\n",
    "\n",
    "    STEPS_PER_EPOCH=1000\n",
    "    VALIDATION_STEPS=50\n",
    "\n",
    "    IMAGE_MAX_DIM=320\n",
    "    IMAGE_MIN_DIM=320\n",
    "    \n",
    "    BACKBONE = model_type # [resnet50, mobilenet224v1]\n",
    "    \n",
    "config = MappingChallengeConfig()\n",
    "config.display()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Instantiate Model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you changed `mrcnn/model.py`, you have to explicitly tell jupyter to reload the module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Original import was: from mrcnn import model as modellib, utils\n",
    "importlib.reload(modellib) # force reload"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = modellib.MaskRCNN(mode=\"training\", config=config, model_dir=LOGS_DIRECTORY)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load pretrained weights\n",
    "model_path = PRETRAINED_MODEL_PATH\n",
    "model.load_weights(model_path, by_name=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load Training and Validation Dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load training dataset\n",
    "dataset_train = MappingChallengeDataset()\n",
    "print(TRAINING_SET_PATH)\n",
    "dataset_train.load_dataset(dataset_dir=TRAINING_SET_PATH, load_small=True)\n",
    "dataset_train.prepare()\n",
    "\n",
    "# Load validation dataset\n",
    "dataset_val = MappingChallengeDataset()\n",
    "val_coco = dataset_val.load_dataset(dataset_dir=VAL_SET_PATH, load_small=True, return_coco=True)\n",
    "dataset_val.prepare()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Train\n",
    "\n",
    "Each epoch of stage 1 takes ~7mins on a GTX 1060. So 40 epochs will take 4.6hrs..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# *** This training schedule is an example. Update to your needs ***\n",
    "\n",
    "# Training - Stage 1\n",
    "print(\"Training network heads\")\n",
    "model.train(dataset_train, dataset_val,\n",
    "            learning_rate=config.LEARNING_RATE,\n",
    "            epochs=40,\n",
    "            layers='heads')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Training - Stage 2\n",
    "# Finetune layers from ResNet stage 4 and up\n",
    "if config.BACKBONE == \"mobilenet224v1\":\n",
    "    stage_2_layers = '11M+'\n",
    "else:\n",
    "    stage_2_layers = '4+'\n",
    "\n",
    "print(\"Fine tune Resnet stage 4 and up\")\n",
    "model.train(dataset_train, dataset_val,\n",
    "            learning_rate=config.LEARNING_RATE,\n",
    "            epochs=120,\n",
    "            layers=stage_2_layers)\n",
    "\n",
    "# Training - Stage 3\n",
    "# Fine tune all layers\n",
    "print(\"Fine tune all layers\")\n",
    "model.train(dataset_train, dataset_val,\n",
    "            learning_rate=config.LEARNING_RATE / 10,\n",
    "            epochs=160,\n",
    "            layers='all')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can monitor the training by running : \n",
    "```\n",
    "tensorboard --logdir=logs/[path-to-your-experiment-logdir]\n",
    "```\n",
    "and if everything works great, you should see something like : \n",
    "![loss-plot](images/loss-plot.png)\n",
    "\n",
    "# Author\n",
    "Sharada Mohanty [sharada.mohanty@epfl.ch](sharada.mohanty@epfl.ch)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
