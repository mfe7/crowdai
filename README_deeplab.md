# Deeplab installation: From TensorFlow Models
## Co-authors: Bjorn Lutjens

## Installation
Download tensorflow/models
Delete everything except research/deeplab and research/slim
Create AWS instance (e.g. g3.8xlarge, 150GB storage)

### Set ENV vars for every new terminal
```
AWS_SSH_ADDRESS="ec2-18-232-123-147.compute-1.amazonaws.com" 
AWS_ROOT="/home/$USER/Desktop/acl/aws" # Path of aws key
PROJECT_NAME="crowdai"
PROJECT_ROOT="/home/$USER/Desktop/$PROJECT_NAME/"
```

### Pass folder from computer to AWS (zip datasets with many files) 
```
ssh -i $AWS_ROOT/bjorn.pem ubuntu@$AWS_SSH_ADDRESS 'mkdir -p /home/ubuntu/'$PROJECT_NAME
scp -i $AWS_ROOT/bjorn.pem -r $PROJECT_ROOT/models/research/. ubuntu@$AWS_SSH_ADDRESS:/home/ubuntu/$PROJECT_NAME/
```

### Connect with ssh into AWS
```
ssh -L localhost:6023:localhost:6006 -L localhost:8823:localhost:8888 -i $AWS_ROOT/bjorn.pem ubuntu@$AWS_SSH_ADDRESS
```

### Build docker in AWS terminal
```
cd /home/ubuntu/crowdai/deeplab/
source build_deeplab_docker.sh
```
### Start docker and add directory to python path
```
source run_deeplab_docker.sh
cd home/ubuntu/crowdai/
export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim
```
### Test tensorflow implementation
```
python deeplab/model_test.py -v
```
### Download and set pretrained checkpoint
```
cd /home/ubuntu/crowdai
CURRENT_DIR=$(pwd)
WORK_DIR="${CURRENT_DIR}/deeplab"
DATASET_DIR="datasets"
INIT_FOLDER="${WORK_DIR}/${DATASET_DIR}/crowdai_data/init_models"
mkdir -p ${INIT_FOLDER}

TF_INIT_ROOT="http://download.tensorflow.org/models"
CKPT_NAME="deeplabv3_mnv2_pascal_train_aug" # MobileNetv2
TF_INIT_CKPT="${CKPT_NAME}_2018_01_29.tar.gz"
cd "${INIT_FOLDER}"
wget -nd -c "${TF_INIT_ROOT}/${TF_INIT_CKPT}"
tar -xf "${TF_INIT_CKPT}"
cd "${CURRENT_DIR}"
```

## Run on MS-coco building crowdai dataset
### Download crowdai datasets
```
cd ~/Desktop/crowdai/mapping-challenge-starter-kit/crowdai/
Download dataset from https://www.crowdai.org/challenges/mapping-challenge/dataset_files
tar -xf *
Set up file structure
-deeplab/datasets
--crowdai_data
--- crowdai_data_voc
---- ImageSets
----- Segmentation
---- JPEGImages
----- train
----- val
---- SegmentationClassRaw 
----- train
----- val
---- exp
--tfrecord
ssh -i $AWS_ROOT/bjorn.pem ubuntu@$AWS_SSH_ADDRESS 'mkdir -p /home/ubuntu/'$PROJECT_NAME'/deeplab/datasets/crowdai_data/crowdai_data_voc/JPEGImages'
ssh -i $AWS_ROOT/bjorn.pem ubuntu@$AWS_SSH_ADDRESS 'mkdir -p /home/ubuntu/'$PROJECT_NAME'/deeplab/datasets/crowdai_data/crowdai_data_voc/SegmentationClassRaw'
ssh -i $AWS_ROOT/bjorn.pem ubuntu@$AWS_SSH_ADDRESS 'mkdir -p /home/ubuntu/'$PROJECT_NAME'/deeplab/datasets/crowdai_data/crowdai_data_voc/ImageSets/Segmentation'
ssh -i $AWS_ROOT/bjorn.pem ubuntu@$AWS_SSH_ADDRESS 'mkdir -p /home/ubuntu/'$PROJECT_NAME'/deeplab/datasets/crowdai_data/tfrecord'
```
####Convert crowdai dataset from MS-coco format to Pascal VOC format:
Run the full notebook for all datasets (can take several hours)
```
mapping-challenge-starter-kit/Dataset Utils.ipynb
```
Copy JPEG images and Segmentation Mask
``` 
DATASET_SPLIT="val" # do for val and train
SEG_PATH_TXT="mapping-challenge-starter-kit/crowdai/crowdai_data/crowdai_data_voc/ImageSets/Segmentation"
SEG_JPEG="JPEGImages" # do for JPEGImages and SegmentationClassRaw
SEG_JPEG_PATH="mapping-challenge-starter-kit/crowdai/crowdai_data/crowdai_data_voc/"${SEG_JPEG}
cd ~/Desktop/crowdai/mapping-challenge-starter-kit/${SEG_JPEG_PATH}
zip -r ${DATASET_SPLIT}.zip ${DATASET_SPLIT}
scp -i $AWS_ROOT/bjorn.pem -r $PROJECT_ROOT/${SEG_JPEG_PATH}/${DATASET_SPLIT}.zip ubuntu@$AWS_SSH_ADDRESS:/home/ubuntu/$PROJECT_NAME/deeplab/datasets/crowdai_data/crowdai_data_voc/${SEG_JPEG}
scp -i $AWS_ROOT/bjorn.pem -r $PROJECT_ROOT/${SEG_PATH_TXT}/${DATASET_SPLIT}.txt ubuntu@$AWS_SSH_ADDRESS:/home/ubuntu/$PROJECT_NAME/deeplab/datasets/crowdai_data/crowdai_data_voc/ImageSets/Segmentation
ssh -i $AWS_ROOT/bjorn.pem ubuntu@$AWS_SSH_ADDRESS 'unzip /home/ubuntu/'${PROJECT_NAME}'/deeplab/datasets/crowdai_data/crowdai_data_voc/'${SEG_JPEG}'/'${DATASET_SPLIT}'.zip -d /home/ubuntu/'${PROJECT_NAME}'/deeplab/datasets/crowdai_data/crowdai_data_voc/'${SEG_JPEG}'/'
```

####Convert Pascal VOC format to tfrecord
```
cd /home/ubuntu/crowdai/deeplab/datasets
DATASET_SPLIT="train" # do for val and train
# This may take a while
python ./build_crowdai_data.py \
  --image_folder="crowdai_data/crowdai_data_voc/JPEGImages/"${DATASET_SPLIT} \
  --semantic_segmentation_folder="crowdai_data/crowdai_data_voc/SegmentationClassRaw/"${DATASET_SPLIT} \
  --list_folder="crowdai_data/crowdai_data_voc/ImageSets/Segmentation" \
  --image_format="jpg" \
  --label_format="png" \
  --output_dir="crowdai_data/tfrecord"
```
### Set environment variables for crowdai dataset
```
TRAIN_LOGDIR_CROWDAI="${WORK_DIR}/${DATASET_DIR}/crowdai_data/exp/train_on_train_set_mobilenetv2/train"
EVAL_LOGDIR_CROWDAI="${WORK_DIR}/${DATASET_DIR}/crowdai_data/exp/train_on_train_set_mobilenetv2/eval"
VIS_LOGDIR_CROWDAI="${WORK_DIR}/${DATASET_DIR}/crowdai_data/exp/train_on_train_set_mobilenetv2/vis"
EXPORT_DIR_CROWDAI="${WORK_DIR}/${DATASET_DIR}/crowdai_data/exp/train_on_train_set_mobilenetv2/export"
CROWDAI_DATASET="${WORK_DIR}/${DATASET_DIR}/crowdai_data/tfrecord"
mkdir -p "${TRAIN_LOGDIR_CROWDAI}"
mkdir -p "${EVAL_LOGDIR_CROWDAI}"
mkdir -p "${VIS_LOGDIR_CROWDAI}"
mkdir -p "${EXPORT_DIR_CROWDAI}"


TF_INIT_CKPT="${WORK_DIR}/${DATASET_DIR}/crowdai_data/train/model.ckpt-10000"
TF_INIT_CKPT="${INIT_FOLDER}/deeplabv3_mnv2_pascal_train_aug/model.ckpt-30000"
```

### Train the model

```
NUM_ITERATIONS=100
python "${WORK_DIR}"/train.py \
  --logtostderr \
  --num_clones=2 \
  --train_split="train" \
  --model_variant="mobilenet_v2" \
  --output_stride=16 \
  --train_crop_size=513 \
  --train_crop_size=513 \
  --train_batch_size=8 \
  --training_number_of_steps="${NUM_ITERATIONS}" \
  --fine_tune_batch_norm=true \
  --tf_initial_checkpoint="${TF_INIT_CKPT}" \
  --train_logdir="${TRAIN_LOGDIR_CROWDAI}" \
  --dataset="crowdai" \
  --base_learning_rate=0.007\
  --dataset_dir="${CROWDAI_DATASET}"
```

  
### Display training progress
```
cd /home/ubuntu/crowdai/deeplab/datasets/crowdai_data
tensorboard --logdir=train
```

### Evaluate the model
```
python "${WORK_DIR}"/eval.py \
  --logtostderr \
  --eval_split="val" \
  --model_variant="mobilenet_v2" \
  --eval_crop_size=513 \
  --eval_crop_size=513 \
  --checkpoint_dir="${TRAIN_LOGDIR_CROWDAI}" \
  --eval_logdir="${EVAL_LOGDIR_CROWDAI}" \
  --dataset_dir="${CROWDAI_DATASET}" \
  --dataset="crowdai"
  --max_number_of_evaluations=1
```

### Visualize the results
```
python "${WORK_DIR}"/vis.py \
  --logtostderr \
  --vis_split="val" \
  --model_variant="mobilenet_v2" \
  --vis_crop_size=513 \
  --vis_crop_size=513 \
  --checkpoint_dir="${TRAIN_LOGDIR_CROWDAI}" \
  --vis_logdir="${VIS_LOGDIR_CROWDAI}" \
  --dataset_dir="${CROWDAI_DATASET}" \
  --dataset="crowdai" \
  --num_clones=2 \
  --max_number_of_iterations=1
```

Copy visualizations back to computer
```
cd ${VIS_LOGDIR_CROWDAI}
zip -r segmentation_results.zip segmentation_results
scp -i $AWS_ROOT/bjorn.pem -r ubuntu@$AWS_SSH_ADDRESS:/home/ubuntu/crowdai/deeplab/datasets/crowdai_data/exp/train_on_train_set_mobilenetv2_lr00007/vis/segmentation_results.zip $PROJECT_ROOT/mapping-challenge-starter-kit/crowdai/crowdai_data/exp/train_on_train_set_mobilenetv2_lr00007/vis
```

### Export the trained checkpoint.
```
NUM_ITERATIONS=30000
CKPT_PATH="${TRAIN_LOGDIR_CROWDAI}/model.ckpt-${NUM_ITERATIONS}"
EXPORT_PATH="${EXPORT_DIR_CROWDAI}/frozen_inference_graph.pb"

python "${WORK_DIR}"/export_model.py \
  --logtostderr \
  --checkpoint_path="${CKPT_PATH}" \
  --export_path="${EXPORT_PATH}" \
  --model_variant="mobilenet_v2" \
  --num_classes=21 \
  --crop_size=513 \
  --crop_size=513 \
  --inference_scales=1.0
```
Copy the exported model to computer
```
scp -i $AWS_ROOT/bjorn.pem -r ubuntu@$AWS_SSH_ADDRESS:/home/ubuntu/crowdai/deeplab/datasets/crowdai_data/exp/train_on_train_set_mobilenetv2/export/frozen_inference_graph.pb $PROJECT_ROOT/models/research/deeplab/datasets/crowdai_data/exp/train_on_train_set_mobilenetv2/export
```

### Run inference on one image 
Comment: This could create dependency issues. Best, to run this in a python3 docker
$PROJECT_ROOT/models/research/deeplab/deeplab_demo.ipynb

## License

[Apache License 2.0](LICENSE)
