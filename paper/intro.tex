%!TEX root=main.tex

\section{Introduction} \label{sec:intro}

In disaster relief scenarios, it is crucial to rapidly assess damage and identify collapsed buildings to allocate rescue workers. Satellite imagery has recently been used to produce accurate pixel maps, as seen in~\cref{fig:example_satellite} of standing buildings of the pre-disaster scenes over huge areas~\cite{crowdAIMappingChallengeBaseline2018}.
After a disaster strikes, first responders could send drones to survey the area faster than humans.
Because satellite images and images from a drone's onboard camera look similar, we propose the use of an image segmentation algorithm to create a post-disaster building map from the drone's aerial imagery. The map can easily be compared with the pre-disaster map to identify images with collapsed buildings.

A key challenge of image segmentation algorithms is slow inference time.
State-of-the-art algorithms typically focus on segmentation accuracy, often thought to require huge neural network models.
However, today's algorithms are too computationally intensive, and slow, to run on a drone's onboard processor in real time~\cite{crowdAIMappingChallengeBaseline2018}.
Although this paper focuses on identifying collapsed buildings, which might be solvable with simpler object detection algorithms, we consider segmentation because we would like to someday expand to label escape routes (roads), which are not as well-described by a bounding box.
We therefore investigated strategies to reduce the inference time of segmentation algorithms on satellite imagery.

Image segmentation algorithms broadly consist of a backbone (feature extractor) and a network head (decision maker).
We started from a baseline implementation~\cite{matterport_maskrcnn_2017} of Mask R-CNN~\cite{he2017mask} with a ResNet50 backbone.
Our first approach considered faster backbones, such as the MobileNet architecture~\cite{howard2017mobilenets}).
The second approach was motivated by a special property of our dataset and task (non-overlapping buildings).
We therefore investigated a reduction in inference time by simply using segmentation, rather than an instance segmentation algorithm.
We expected a tradeoff between model accuracy and inference times.
Our results surprisingly show that the second approach led to both improved accuracy and inference time.

\begin{figure}[t]
	\centering
	\includegraphics [trim=0 500 0 0, clip, angle=0, width=0.7\columnwidth,
	keepaspectratio]{figures/mapping_challenge}
	\caption{Satellite Imagery. A raw satellite image (left) with the regions corresponding to buildings overlayed (right).} 
	\label{fig:example_satellite}
\end{figure}

Our contributions include:
i) an investigation of MobileNet backbones with Mask R-CNN, 
ii) an investigation of DeepLabv3+ as an alternative segmentation algorithm,
and iii) results on a satellite image dataset that show improvements in both inference time and segmentation accuracy.
Our code is at \url{https://bitbucket.org/mfe7/crowdai} with Docker containers for each network for easy reproducibility.