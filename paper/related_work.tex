%!TEX root=main.tex

\section{Related Work} \label{sec:related_work}

\subsection{Semantic Segmentation}
Semantic segmentation is more challenging than image classification, because spatial relationships have to be maintained throughout the network. First approaches learn a semantic map while preserving image dimensions throughout the entire network, but are computationally too expensive. An encoding-decoding architecture, such as U-Net~\cite{ronneberger2015Unet}, showed to be computationally more efficient than networks with unchanging feature dimensions. The U-Net intuitively learns image classifications in low level features and recreates spatial relationships through skip connections to early high-resolution layers. Pooling or strided convolutions are traditionally used to downsample the input into low-resolution layers with high perceptive field, but reduce the spatial resolution or are computationally expensive, respectively. Dilated or atrous convolutions, as used in Deeplab~\cite{chen2018deeplab}, instead downsample the image while maintaining the full field of view and spatial relations. This work explores the usage of Deeplabv3+ for the semantic segmentation of aerial images.

%\mfe{bjorn: various loss fns? whether or not you actually implemented them}
%\mfe{picture dilated conv}

\subsection{Instance Segmentation}
Faster R-CNN~\cite{ren2015FasterRCNN}, uses a standard backbone to extract a feature map from an image, then a network module proposes regions of interest (ROIs), where it thinks objects might be.
The network considers each ROI separately: the image is cropped to the ROI's size and re-scaled to a standard aspect ratio.
The cropped image goes through an image classifier (like MiniPlaces), and a bounding box coordinate predictor.
Given the ground truth object classes and bounding box coordinates, the whole network can be trained with backpropagation.

A breakthrough paper in instance segmentation is Mask R-CNN~\cite{he2017mask}.
The authors propose an intuitive, yet simple, addition to Faster R-CNN, and demonstrate state-of-the-art demonstration on a variety of instance-related tasks.
The key idea is to add a mask prediction output in parallel to the bounding box and classification outputs.
The architecture is designed to decouple mask prediction and classification/bounding box estimation, by predicting masks for every possible class, but only penalizing the network based on the errors in the mask of the predicted class.

\subsection{Efficient Neural Networks}

Reduction in neural network model complexity is often motivated by a desire to implement algorithms on platforms with limited computation power (e.g. mobile phones, self-driving cars).
The two main ideas are to compress trained networks~\cite{chen2015Compression} or to train small and efficient networks from the start~\cite{howard2017mobilenets}.

A major expensive operation contributing to network inefficiency is the convolution.
Standard convolution requires $D_F \cdot D_F \cdot M \cdot N \cdot D_K \cdot D_K$ operations, where $D_F$ is the input feature map dimension, $M$ is the number of channels in the input feature map, $D_K$ is the convolutional kernel dimension, $N$ is the number of convolutional filters (and also the number of output feature channels).

A popular idea is to reduce the expensive operation of convolution by factorizing it into two steps, a process called \textit{depthwise separable convolution}~\cite{sifre2014rigid}.
Depthwise separable convolution begins with a \textit{depthwise convolution}, where each input channel is filtered independently, and then a \textit{pointwise convolution}, where the filtered channels are combined to produce an output.
The depthwise convolution requires only $D_F \cdot D_F \cdot M \cdot D_K \cdot D_K$ operations, because it applies a convolutional kernel of size $D_K \times D_K$ across each pixel in a feature map of size $D_F \times D_F$, over all $M$ feature map channels.
The pointwise convolution only requires $D_F \cdot D_F \cdot M \cdot N$ operations, because it applies a convolutional kernel of size $1 \times 1$ across each pixel in the resulting $D_F \times D_F$ depthwise convolved feature map with $M$ channels, for $N$ different kernels.
There is therefore a reduction in computation of a factor of $\frac{1}{N} + \frac{1}{K^2}$ over the standard convolution.

Depthwise separable convolution is the key idea behind many recent works (e.g. XCeption~\cite{chollet2017xception}, Squeezenet~\cite{iandola2016squeezenet}), including MobileNet\cite{howard2017mobilenets}.
MobileNets are a class of network architectures that stack 13 depthwise separable convolution layers.
For classification, a typical average pooling, fully connected, and softmax layer can attach to the end, but the important idea is the backbone consisting of the convolutions.
MobileNets provide an opportunity to tradeoff latency and accuracy, by introducing a parameter $\alpha \in (0,1]$, where each layer's number of input and output features are scaled down by $\alpha$, reducing the number of parameters roughly by $\alpha^2$.
It is therefore expected that as $\alpha$ decreases (approaches 0), the model will become smaller, faster, and less accurate.

