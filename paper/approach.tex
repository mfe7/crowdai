%!TEX root=main.tex

\section{Approach} \label{sec:approach}

The high level objective of this paper is to eventually enable real-time segmentation on the limited computation available on a small drone.
Existing image segmentation algorithms typically require a desktop GPU, and inference times are still slow.
We therefore considered approaches to reduce the inference time of the image segmentation module.

We investigated two strategies: faster backbones in an instance segmentation algorithm, and leveraging unique properties of our domain to re-frame the task as a simpler, semantic segmentation problem.

\subsection{Baseline: Mask R-CNN}
We started with an open-source implementation of Mask R-CNN~\cite{crowdAIMappingChallengeBaseline2018} (heavily based on~\cite{matterport_maskrcnn_2017}) with a ResNet-50 backbone.
This implementation also included network weights that gave good performance.
It included training code, but did not describe the details of training in sufficient detail (e.g. network initialization strategy), which made it difficult to reproduce.

To gain familiarity with the dataset and baseline, we replicated the implementation on our own machines, and were able to confirm the advertised results of the frozen model.
We then trained the network, starting from the frozen model, but the performance declined, suggesting the frozen model was already well-trained on this dataset.

% Training params:
% SGD w/ learning rate , L2 regularization w/ weight

The loss function was $L = L_{mask} + L_{class} + L_{bbox}$, where $L_{mask}$ is the binary cross-entropy loss of the predicted class's mask in each propsed ROI against the true mask, $L_{class}$ is the cross-entropy loss of each predicted ROI's class against the true class, and $L_{bbox}$ is a smooth $L_1$ loss applied to the bounding box of the predicted class, as in the Mask R-CNN paper.

\subsection{MobileNet Backbones}
We discovered an open pull request on the Matterport Mask R-CNN repository~\cite{matterport_maskrcnn_2017} that extended the algorithm to use a MobileNet backbone (instead of ResNet).
That pull request was a streamlined form of the public Keras MobileNet Python implementation~\cite{mobilenet_keras_github}.
We ported the major changes from this pull request into our repository.
We also extended the implementation to accept varying $\alpha$ values, which control the latency/accuracy tradeoffs described in the paper. 

The backbone's purpose is to extract low- and high-level features from the raw image, and the network's head (i.e. bounding box, class, and mask prediction modules) combines these features to make decisions.
We initialized the MobileNet backbone's weights with the ones found online~\cite{mobilenet_imagenet_weights} that were trained on ImageNet~\cite{imagenet_cvpr09}.
The low-level features in ImageNet images (e.g. edges, shapes) are likely not too different from the low-level features in satellite images.
Therefore our first training strategy was to freeze the backbone weights, and only backpropagate errors back through the network heads.
In this way, we would not waste time updating a well-trained backbone.

However, we also suspected that the high-level features between the two datasets are likely different, meaning it may make sense to update a larger chunk of the network.
Accordingly, we also trained through the entire network and compared the performance of the two approaches.
For completeness, we also evaluated the performance of the network pre-trained on ImageNet with no additional training on our dataset, just to confirm that the training process improved the performance.

Since MobileNet is really a collection of backbones, parameterized by $\alpha$, we compared between $\alpha = 1.0$ (slowest, most accurate) and $\alpha = 0.25$ (fastest, least accurate).

\subsection{DeepLab Semantic Segmentation}
Parallely to exchanging the backbone in an instance segmentation we have implemented the semantic segmentation algorithm DeepLabv3+ with MobileNet-v2 backbone. We started with an open-source implementation of DeepLabv3+~\cite{chen2018deeplab} and heavily based it off~\cite{deeplab_git}.

We first converted 280k training and 60k validation images from the given MS-COCO format to VOC PASCAL format. The open-source DeepLab code takes as input a pixel-wise ground truth segmentation map. A script was developed from scratch that generates this map, based off an polygonal representations of segmentation maps from the MS-COCO format. 

To enable reproducibility of the code and implement a running baseline, the authors implemented a Dockerfile for the DeepLabv3+ implementation. This Dockerfile enables one-click reproducibility among different computers and will be submitted to the open-source deeplab implementation. 

The original implementation of DeepLab uses an Xception backbone, which has been exchanged for a MobileNet-v2 backbone to increase inference time. The MobileNet-v2 backbone has been pretrained on the VOC2012 dataset and performed poorly on the satellite imagery dataset~\cref{tab:latency_and_accuracy}. Next, the model's head, backbone and batch norm layers have been fine-tuned on the converted satellite image training dataset with the default starting learning rate, which decays over training time. The authors suspected that the base learning rate was too low and increased it. A model with the higher learning rate, also initialized on VOC2012, presumably achieved to exscape the local minima from the VOC2012 dataset and achieved impressive performance on the semantic segmentation task~\cref{tab:latency_and_accuracy}. The model uses cross entropy loss, whereas future works discovers the usage of distance weighted cross entropy loss to foster learning of sharp building edges.

\subsection{Implementation Details}
We trained the Mask R-CNN + MobileNet-v1 backbone on a desktop PC with a GTX 1060 6GB.
This limited the batch size to 1, as opposed to 16 in the original Mask R-CNN paper (2 images for each of 8 GPUs).
Learning rate $\lambda=$ 0.001, SGD optimizer with momentum = 0.9, weight decay = 0.0001.
Other parameters listed in \texttt{config.py} in our repository.

The DeepLabv3+ with MobileNet-v2 and learning rate $\lambda = 1e^{-4}$ and $\lambda = 7e^{-3}$ was trained on an Amazon AWS g3.8xlarge instance with batch size 8 and g3.4xlarge instance with batch size 4, respectively.
Running it on AWS instances, allowed to train two model clones on each AWS instance's GPU. The tensorflow implementation uses a the Momentum optimization algorithm with a learning rate that decays by a factor of 0.1 every 2000 steps.


%Diff mobilenetv1 and v2 ALSO diff mobilenet and xception backbone
%explanation atrous convolution
%TODO: change atrous convolution rate for performance latency trade-off
%fine tuning batch norm
%loss function
%%IOU comparison
%Amazon AWS g3.8xlarge 
%visualization of models with 
