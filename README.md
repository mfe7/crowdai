### 6.869 Final Project ###


#### Download the dataset ####
First download the dataset (images+annotations) from https://www.crowdai.org/challenges/mapping-challenge/dataset_files.

I expanded the .tar.gz for the following directory structure
```
data
|__test_images/
|__train/
|____images/
|____annotations.json
|__val/
|____images/
|____annotations.json
```

Grab the pre-trained model from https://www.crowdai.org/challenges/mapping-challenge/dataset_files and for mobilenet initialized on imagenet: https://github.com/fchollet/deep-learning-models/releases/download/v0.6/mobilenet_1_0_224_tf_no_top.h5 and https://github.com/fchollet/deep-learning-models/releases/download/v0.6/mobilenet_2_5_224_tf_no_top.h5, put them in a folder as follows:
```
pretrained_models
|__pretrained_weights.h5 # resnet50 backbone for mask r-cnn from crowdai
|__mobilenet_1_0_224_tf_no_top.h5 # mobilenet backbone (alpha = 1.0)
|__mobilenet_2_5_224_tf_no_top.h5 # mobilenet backbone (alpha = 0.25)
```

#### Clone the repo ####
```
mkdir ~/code
cd ~/code
git clone https://mfe7@bitbucket.org/mfe7/crowdai.git

# If SSH keys...
git clone git@bitbucket.org:mfe7/crowdai.git
```

#### Clone my docker image repo ####
```
cd ~/code
git clone https://mfe7@bitbucket.org/mfe7/docker_images.git

# git clone git@bitbucket.org:mfe7/docker_images.git
```

Per the instructions in the `docker_images/README.md`, add this to your `~/.bashrc`:
```
DOCKER_IMG_DIR="$HOME/code/docker_images"
if [ -d "$DOCKER_IMG_DIR" ]; then
	for dir in `ls $DOCKER_IMG_DIR`;
	do
	    if [ $dir != "README.md" ]; then
            source $DOCKER_IMG_DIR/$dir/commands.sh;
        fi
	done
fi
```

#### Build & Run the Docker image ####

```
source ~/.bashrc
docker_build_crowdai
docker_run_crowdai
```

In the future, if you haven't changed the Dockerfile (at `~/code/docker_images/crowdai/Dockerfile`), you can simply:
```
docker_run_crowdai
```

#### Run the notebook ####
In the Docker container, start a notebook with proper permissions/ports:
```
jupyter notebook --ip 0.0.0.0 --no-browser --allow-root
```

It will print out a URL that you can visit in a browser to view the notebook.

I've been using the `Prediction and Submission.ipynb` notebook. It currently loads the pretrained network file, runs the inference on the validation set (takes 1.5 hrs), then outputs the precision/recall.

During training, using `Training.ipynb`, you can view the progress in tensorboard run locally (outside the docker):
```
tensorboard --logdir=~/code/crowdai/crowdai-mapping-challenge-mask-rcnn/logs
```

To profile keras model... do this in the docker container once.
```
apt install cuda-toolkit-9-0 # installs cupti (an extra cuda tool)
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/extras/CUPTI/lib64/
```

#### Train the Model ####

The `Training.ipynb` notebook will walk you through training, but for a simple python script, please use: 
```
python3 train.py
```

#### Generating Results ####

There are a few scripts we wrote to compute statistics for MRCNN models:

`evaluate_inference_time.py`, `evaluate_iou.py`, `get_paper_images.py`, `mrcnn_profiler.py` are named according to the statistic they compute.


Michael Everett - Dec 12, 2018 - 6.869 project